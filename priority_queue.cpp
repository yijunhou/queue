// Example program
#include <iostream>
#include <string>
#include <queue>
using namespace std;

int main()
{
  priority_queue<int, vector<int>, less<int>> q1; // greater
  q1.push(10);
  q1.push(0);
  cout << q1.top(); // 10
}

// constructor: make_heap O(n)
// push: push_heap O(logn)
